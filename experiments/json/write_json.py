import json
import uuid

testStructure = {
    'things': [
        {
            'id': str(uuid.uuid4()),
            'name': 'blarg'
        },
        {
            'id': str(uuid.uuid4()),
            'name': 'another'
        },
        {
            'id': str(uuid.uuid4()),
            'name': 'one more'
        }
    ]
}

filename = 'jsontest.json'
fp = open(filename, 'w')
try:
    json.dump(testStructure, fp)
    print 'I think that did it?  Look for file: "{0}"'.format(filename)
finally:
    fp.close()
