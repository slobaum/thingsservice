import unittest
import os, sys
sys.path.append(os.path.abspath('../'))
from repos.RepoBase import RepoBase
from TestJsonProvider import TestJsonProvider

class RepoBaseTest(unittest.TestCase):

    def setUp(self):
        self.repo = RepoBase(TestJsonProvider())

        self.repo._set('testString', 'a string')
        self.repo._set('testNumber', 42)
        self.repo._set('testCollection', [])
        self.repo._set('testDictionary', {
            'dictString': 'value in dictionary',
            'dictNumber': 42,
            'dictNested': {}
        })

    def tearDown(self):
        self.repo = None

    def testSetGet(self):
        self.assertEqual(
            self.repo._get('testString'),
            'a string',
            'Unable to get String value from repo'
        )
        self.assertEqual(
            self.repo._get('testNumber'),
            42,
            'Unable to get Number value from repo'
        )
        self.assertEqual(
            type(self.repo._get('testCollection')),
            list,
            'Unable to get collection from repo'
        )
        testDict = self.repo._get('testDictionary')
        self.assertEqual(
            type(testDict),
            dict,
            'Unable to get dictionary from repo'
        )
        self.assertEqual(
            testDict['dictString'],
            'value in dictionary',
            'Unable to get string value from dictionary retrieved from repo'
        )
        self.assertEqual(
            testDict['dictNumber'],
            42,
            'Unable to get number value from dictionary retrieved from repo'
        )
        self.assertEqual(
            type(testDict['dictNested']),
            dict,
            'Unable to get nested dictionary from dictionary retrieved from repo'
        )
    
    def testAppend(self):
        with self.assertRaises(KeyError):
            self.repo._append('testString', 'value')
        
        self.repo._append('testCollection', 'one')
        self.repo._append('testCollection', 'two')
        self.repo._append('testCollection', 'three')

        collection = self.repo._get('testCollection')

        self.assertEqual(type(collection), list)
        self.assertEqual(len(collection), 3)
        self.assertEqual(collection[0], 'one')
        self.assertEqual(collection[1], 'two')
        self.assertEqual(collection[2], 'three')

        self.repo._append('testCollection', 'four')
        
        self.assertEqual(
            self.repo._get('testCollection')[3],
            'four'
        )

if __name__ == '__main__':
    unittest.main()