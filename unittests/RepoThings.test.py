import unittest
import os, sys
sys.path.append(os.path.abspath('../'))
from repos.RepoThings import RepoThings
from TestJsonProvider import TestJsonProvider

class RepoThingsTest(unittest.TestCase):

    def setUp(self):
        self.repo = RepoThings(TestJsonProvider())

    def tearDown(self):
        self.repo = None

    def testAddThing(self):
        self.repo.addThing({
            'name': 'testThingOne',
            'list': [ 'one', 'two', 'three' ]
        })
        self.repo.addThing({
            'name': 'testThingTwo',
            'list': [ 'four', 'five', 'six' ]
        })
        self.repo.addThing({
            'name': 'testThingThree',
            'list': [ 'seven', 'eight', 'nine' ]
        })

    def testGetAllThings(self):
        things = self.repo.getThings()
        
        self.assertEqual(
            type(things),
            list,
            'Unable to get collection of things'
        )
        self.assertEqual(
            len(things),
            3,
            'List of things is incorrect length'
        )