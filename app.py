#!flask/bin/python
import os, sys
from flask import Flask, jsonify, make_response
import repos
import routes
from JsonSync import JsonSync

app = Flask(__name__)
config = {
    'host': os.environ['IP'],
    'port': int(os.environ['PORT']),
    'serviceUrl': '/anything/v1/',
    'jsonStorePath': './storage/storage.json'
}

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

jsonProvider = JsonSync(config['jsonStorePath'])

routes.RouteService(app, config['serviceUrl']).getRoutes()
routes.RouteThings(
    app,
    config['serviceUrl'],
    repos.RepoThings(jsonProvider)
).getRoutes()

if __name__ == '__main__':
    app.run(
        host = config['host'],
        port = config['port'],
        debug = True
    )