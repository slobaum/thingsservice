from flask import Flask, jsonify

class RouteBase(object):

    def __init__(self, app, serviceBaseUrl, repo = None):
        self.app = app
        self.serviceBaseUrl = serviceBaseUrl
        self.repo = repo

    def _feedback(self, text):
        print "Creating route: '{0}'".format(text)

    def getRoutes(self):
        raise NotImplementedError('Routes should implement method "getRoutes"')