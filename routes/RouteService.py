from flask import Flask, jsonify
from RouteBase import RouteBase

class RouteService(RouteBase):

    def getRoutes(self):

        self._feedback('/ [GET]')
        @self.app.route('/')
        def index():
            return u"Application Running - service available at '{0}'".format(self.serviceBaseUrl)
        
        self._feedback(self.serviceBaseUrl + ' [GET]')
        @self.app.route(self.serviceBaseUrl, methods=['GET'])
        def service_index():
            return jsonify({
                'service': u"Service description here"
            })