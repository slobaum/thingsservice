from flask import Flask, jsonify, abort
from RouteBase import RouteBase

class RouteThings(RouteBase):

    def getRoutes(self):

        self._feedback(self.serviceBaseUrl + 'things [GET]')
        @self.app.route(self.serviceBaseUrl + 'things', methods=['GET'])
        def get_things():
            things = self.repo.getThings()
            if things:
                return jsonify(things)
            else:
                abort(404)

        self._feedback(self.serviceBaseUrl + 'things [POST]')
        @self.app.route(self.serviceBaseUrl + 'things', methods=['POST'])
        def post_thing():
            print "Post things here"
            