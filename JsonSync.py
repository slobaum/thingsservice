import json
import os.path

class JsonSync(object):

    def __init__(self, filePath):
        self.ensurePath()
        self.filePath = filePath

    def ensurePath(self):
        dirName = os.path.dirname(self.filePath)
        if not os.path.exists(dirName):
            os.makedirs(dirName)
        if not os.path.isfile(self.filePath):
            pointer = open(self.filePath, 'w')
            try:
                json.dump({}, pointer)
            finally:
                pointer.close()
    
    def getData(self):
        self.ensurePath()
        pointer = open(self.filePath, 'r')
        data = {}
        try:
            data = json.load(pointer)
        finally:
            pointer.close()
        return data if data else {}
    
    def persist(self, data):
        pointer = open(self.filePath, 'w')
        try:
            json.dump(data, pointer)
        except:
            raise IOError('Unable to persist data to disk!')
        finally:
            pointer.close()