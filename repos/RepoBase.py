import uuid

class RepoBase(object):

    def __init__(self, jsonProvider):
        self.json = jsonProvider

# protected methods
    def _ensureId(self, entity, idKey = 'id'):
        # TODO:  Add tests to RepoBase.test
        if not idKey in entity.keys():
            entity[idKey] = str(uuid.uuid4())
        return entity

    def _get(self, key):
        self.__refreshData()
        if key in self._data.keys():
            return self._data[key]
        else:
            return None

    def _set(self, key, value):
        self.__refreshData()
        self._data[key] = value
        self.json.persist(self._data)

    def _append(self, key, item):
        self.__refreshData()
        collection = self._get(key)
        if not collection:
            collection = []
        elif not isinstance(collection, (frozenset, list, set, tuple)):
            raise KeyError('Attempting to append to non-collection')
        collection.append(item)
        self._set(key, collection)

# private methods
    def __refreshData(self):
        self._data = self.json.getData()
