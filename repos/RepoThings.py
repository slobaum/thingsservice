from RepoBase import RepoBase

class RepoThings(RepoBase):
    dataKey = 'things'

    def getThings(self):
        return self._get(self.dataKey)

    def addThing(self, thing):
        if not isinstance(thing, (dict, object)):
            raise TypeError('Things are expected to be a dictionary or object')
        self._ensureId(thing)
        self._append(self.dataKey, thing)